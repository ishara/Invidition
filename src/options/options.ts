// @ts-ignore
import ISO6391 from "iso-639-1";
import * as _ from "lodash";
import {iso3166} from "../lib/iso3166";
// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import * as InvidiousAPI from "../lib/API/invidious";
import {
    addSpinIcon,
    generateInvidiousInstancesDropdown,
    initTor,
    removeSpinIcon,
    saveForm,
    selectFastestInstance,
    toggleCustomInstance,
    updateDOM,
} from "../lib/form";
import {error} from "../lib/helpers";
import * as InternationalizationManager from "../lib/i18n";
import * as LocalizationManager from "../lib/localization";
import * as SettingsManager from "../lib/settings";
import {resetSettings} from "../lib/settings";

const generateCaptionsDropdowns = async () => {
    try {
        const defaultCaptions = document.getElementsByClassName("defaultCaptions");

        _.forEach(defaultCaptions, (element) => {
            const defaultOption: HTMLOptionElement = document.createElement("OPTION") as HTMLOptionElement;
            defaultOption.value = "";
            defaultOption.selected = true;

            element.append(defaultOption);

            _.forEach(ISO6391.getAllCodes(), (code) => {
                const option: HTMLOptionElement = document.createElement("OPTION") as HTMLOptionElement;
                option.value = code;
                option.innerText = ISO6391.getNativeName(code);

                element.append(option);
            });
        });
    } catch (e) {
        error(e.message);
    }
};

const generateInterfaceLanguages = async () => {
    try {
        const settings = await SettingsManager.load();

        _.forEach(settings.invidious.appSettings.availableLanguages, (languageTag) => {
            if (browser.i18n.getUILanguage() === LocalizationManager.normalizeLanguageCodeToBrowser(languageTag)) {
                settings.invidious.instanceSettings.hl = languageTag;
            }
        });

        const defaultOption = document.createElement("OPTION") as HTMLOptionElement;
        defaultOption.value = "";
        defaultOption.selected = true;

        document.getElementById("invidious-instanceSettings-hl").append(defaultOption);

        _.forEach(settings.invidious.appSettings.availableLanguages, (languageTag) => {
            const el = document.createElement("OPTION") as HTMLOptionElement;
            el.value = languageTag;
            el.innerText = LocalizationManager.trimLocaleSubtag(languageTag);
            document.getElementById("invidious-instanceSettings-hl").append(el);
        });
    } catch (e) {
        error(e.message);
    }
};

const generateRegions = async () => {
    try {
        const select = document.getElementById("invidious-instanceSettings-region");

        select.append(document.createElement("OPTION"));

        _.forEach(iso3166, (o) => {
            const el = document.createElement("OPTION") as HTMLOptionElement;
            el.value = o["alpha-2"];
            el.innerText = o.name;
            select.append(el);
        })
    } catch (e) {
        console.error(e.message);
    }
};

const init = async () => {
    try {
        await initTor();

        const settings = await SettingsManager.load();

        if (await InvidiousAPI.isCustomInstance(settings.invidious.appSettings.instance)) {
            await toggleCustomInstance();
        }

        InternationalizationManager.replaceDOM();

        await generateCaptionsDropdowns();
        await generateInterfaceLanguages();
        await generateRegions();
        updateDOM();
        generateInvidiousInstancesDropdown();

        document.getElementById("invidition-form").addEventListener("change", saveForm);
        document.getElementById("invidious-button-fastest-instance").addEventListener("click", selectFastestInstance);
        document.getElementById("invidious-button-custom-instance").addEventListener("click", toggleCustomInstance);
        document.getElementById("invidious-button-random-instance").addEventListener("click", selectRandomInstance);
        document.getElementById("invidition-resetSettings").addEventListener("click", reset);
    } catch (e) {
        error(e.message);
    }
};

const selectRandomInstance = async (e) => {
    try {
        const buttonValue = addSpinIcon(e.target);
        const instance = await InvidiousAPI.getRandomInstance();

        const dropdown = document.getElementById("invidious-appSettings-instance") as HTMLSelectElement;

        dropdown.value = instance[0];
        await saveForm();

        removeSpinIcon(e.target, buttonValue);
    } catch (e) {
        error(e.message);
    }
};

const reset = async (e) => {
    try {
        e.preventDefault();
        await resetSettings();

        updateDOM();

        const defaultCaptions = document.getElementsByClassName("defaultCaptions");

        _.forEach(defaultCaptions, (c: HTMLOptionElement) => {
            c.value = "";
        });
    } catch (e) {
        error(e.message);
    }
};

try {
    document.addEventListener("DOMContentLoaded", init);
} catch (e) {
    error(e);
}
