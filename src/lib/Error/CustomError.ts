export default class CustomError extends Error {
    constructor(message?: string) {
        // 'Error' breaks prototype chain here
        super(message);

        // Restore prototype chain
        const actualProto = new.target.prototype;

        if (Object.setPrototypeOf) {
            Object.setPrototypeOf(this, actualProto);
        } else {
            // @ts-ignore
            this.__proto__ = actualProto;
        }
    }
}
