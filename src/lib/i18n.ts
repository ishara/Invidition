import * as _ from "lodash";
// @ts-ignore
import {browser} from "webextension-polyfill-ts";

export const replaceDOM = () => {
    try {
        replaceTitle();
        replaceContent();
    } catch (e) {
        console.error(e.message);
    }
};

export const replaceContent = () => {
    try {
        _.forEach(document.querySelectorAll("[data-i18n-content]"), (el: HTMLElement) => {
            const messageName = el.getAttribute("data-i18n-content");

            el.innerText = browser.i18n.getMessage(messageName);
        });
    } catch (e) {
        console.error(e.message);
    }
};

export const replaceTitle = () => {
    try {
        _.forEach(document.querySelectorAll("[data-i18n-title]"), (el: HTMLElement) => {
            const messageName = el.getAttribute("data-i18n-title");

            el.setAttribute("title", browser.i18n.getMessage(messageName));
        });
    } catch (e) {
        console.error(e.message);
    }
};
