import {hostname} from "os";
import {domainToUrl} from "../helpers";
import * as SettingsManager from "../settings";
import * as _ from "lodash";

/**
 * Generate the Bibliogram Url
 *
 * @param {URL} oldUrl      The URL to apply settings
 * @returns {Promise<URL>}
 */
export const generateUrl = async (oldUrl: URL) => {
    try {
        const settings = await SettingsManager.load();
        const knownPaths = [
            "",
            "a",
            "admin",
            "api",
            "favicon.ico",
            "static",
            "imageproxy",
            "p",
            "u",
            "developer",
            "about",
            "legal",
            "explore",
            "directory",
            "index.html", // Workaround for snopyta. @TODO remove once the official landing page gets out
        ];

        let newUrl = domainToUrl(settings.bibliogram.appSettings.instance);

        const splitPath = oldUrl.pathname.split("/");
        const mainPath = splitPath[1] || "";

        if (_.endsWith(oldUrl.hostname, "cdninstagram.com")) {
            newUrl.pathname = `/imageproxy`;
            newUrl.search = `?url=${encodeURIComponent(oldUrl.href)}`;
        } else if (!_.includes(knownPaths, mainPath)) { // If we are trying to reach an user page
            newUrl.pathname = `/u/${encodeURIComponent(mainPath)}`;
        } else {
            newUrl.pathname = oldUrl.pathname;
            newUrl.search = oldUrl.search;
        }

        return newUrl;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 *
 * @param r
 * @returns {Promise<boolean>}
 */
export const mustRedirect = async (r) => {
    try {
        const appSettings = (await SettingsManager.load()).bibliogram.appSettings;

        if (!appSettings.isEnabled) {
            return false;
        }

        const oldUrl = new URL(r.url);
        let newUrl = new URL(r.url);
        newUrl = await generateUrl(newUrl);

        return newUrl.hostname !== appSettings.instance || oldUrl.href !== newUrl.href;

    } catch (e) {
        console.error(e.message);
    }

    return false;
};
